const getStatus = async function (event: Event) {
  const { apiUrl } = useRuntimeConfig();

  const status = await $fetch(apiUrl + 'status', {
    method: 'GET',
    headers: {
      'content-type': 'application/json',
      'authorization': event.req.headers.authorization,
    },
  })
    .catch((error) => {
      return error;
    });

  return status['hydra:member'];
};

export default defineEventHandler(getStatus);
