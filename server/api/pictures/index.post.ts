const postPicture = async function(event) {
    const { apiUrl } = useRuntimeConfig();
    const body = await readBody(event);

    return await $fetch(apiUrl + 'pictures', {
      method: 'POST',
      headers: {
        'content-type': 'application/json',
        'authorization': event.req.headers.authorization
      },
      body: body
    });
}

export default defineEventHandler(postPicture);
