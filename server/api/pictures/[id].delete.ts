const deletePicture = async function(event) {
    const { apiUrl } = useRuntimeConfig();
    const { id } = event.context.params;

    return await $fetch(apiUrl + 'pictures/' + id, {
      method: 'DELETE',
      headers: {
        'content-type': 'application/json',
        'authorization': event.req.headers.authorization
      }
    });
}

export default defineEventHandler(deletePicture);
