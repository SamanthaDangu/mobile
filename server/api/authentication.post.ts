const authenticate = async function(event) {
    const { apiUrl } = useRuntimeConfig();
    const body = await readBody(event);

    return await $fetch(apiUrl + 'authentication', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: body
    });
}

export default defineEventHandler(authenticate);
