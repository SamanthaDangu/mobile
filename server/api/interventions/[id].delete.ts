const deleteIntervention = async function (event) {
    const { apiUrl } = useRuntimeConfig();
    const { id } = event.context.params;

    return await $fetch(apiUrl + 'interventions/' + id, {
        method: 'DELETE',
        headers: {
            'content-type': 'application/json',
            'authorization': event.req.headers.authorization
        }
    });
}

export default defineEventHandler(deleteIntervention);
