export default interface Employer {
    id: string;
    siren: string;
    name: string;
    longitude: string;
    latitude: string;
}