export default interface Location {
    id: string;
    street: string;
    rest: string;
    postcode: string;
    city: string;
    longitude: string;
    latitude: string;
}