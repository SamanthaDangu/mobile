export default interface Status {
    id: number;
    name: string;
    slug: string;
    pressed: boolean;
}