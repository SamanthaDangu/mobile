export default interface Category {
    id: number;
    name: string;
    pressed: boolean;
}