export default interface Priority {
    id: number;
    name: string;
    slug: string;
    pressed: boolean;
}