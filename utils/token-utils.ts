import jwt_decode from 'jwt-decode';

export interface DecodedToken {
    employerId: Number;
    userId: Number;
    roles: String[];
}

export function getTokenCookie() {
    return useCookie('aei-mobile-token');
}

export function getToken() {
    const token = getTokenCookie();
    if(token ===  undefined){
        return null;
    }
    return token.value;
}

export function getDecodedToken(token: string): DecodedToken {
    return jwt_decode(token);
}

export function getCurrentEmployerId(decodedToken: DecodedToken): Number {
    return decodedToken.employerId;
}

export function getCurrentUserId(decodedToken: DecodedToken): Number {
    return decodedToken.userId;
}